# frozen_string_literal: true

# A user of the application.
#
# Users can be authenticated with their email address and password. There can
# only be one user per email address.
class User < ApplicationRecord
  has_secure_password

  validates :email, presence: true, uniqueness: { case_sensitive: false }
  validates :password, length: { minimum: 8 }

  before_save { self.email = email.downcase }

  class << self
    def authenticate(email, password)
      find_by(email:)&.authenticate(password) || nil
    end
  end
end
