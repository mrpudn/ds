# frozen_string_literal: true

# Base class for the application's models
class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class
end
