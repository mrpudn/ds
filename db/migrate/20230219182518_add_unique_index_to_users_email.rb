# frozen_string_literal: true

# This unique index was missed when creating the users table. This index will
# improve performance when selecting users by email (ex. when logging in).
# Additionally, there can only be one user per email, so these values should be
# unique within the database.
class AddUniqueIndexToUsersEmail < ActiveRecord::Migration[7.0]
  def change
    add_index :users, :email, unique: true
  end
end
