# frozen_string_literal: true

# These `NOT NULL` constraints were missed when creating the users table. Users
# should always have an email address and password.
class AddNotNullConstraintsToUsers < ActiveRecord::Migration[7.0]
  def change
    change_column_null(:users, :email, false)
    change_column_null(:users, :password_digest, false)
  end
end
