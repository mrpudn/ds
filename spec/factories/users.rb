# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { 'email@example.com' }
    password { 'net=vast&infinite' }
  end
end
